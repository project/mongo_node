<?php

/**
 * @file
 * Default settings file for Mongo Node. Edit directly or via UI.
 */

$settings = array(
  'mongo_article' =>
  array(
    'label' => t('Article'),
    'bundles' =>
    array(
      'article_per_link' =>
      array(
        'label' => t('Article per link'),
        'description' => '',
        'view modes' => array(
          'full' => array(
            'label' => 'Full content',
            'custom settings' => FALSE,
          ),
          'teaser' => array(
            'label' => 'Teaser',
            'custom settings' => TRUE,
          ),
        ),
      ),
      'article_per_description' =>
      array(
        'label' => t('Article per template'),
        'description' => '',
        'view modes' => array(
          'full' => array(
            'label' => 'Full content',
            'custom settings' => FALSE,
          ),
          'teaser' => array(
            'label' => 'Teaser',
            'custom settings' => TRUE,
          ),
        ),
      ),
    ),
    'fieldable' => TRUE,
  ),
  'company' =>
  array(
    'label' => t('Company'),
    'bundles' =>
    array(
      'company' =>
      array(
        'label' => t('Company'),
        'description' => '',
        'view modes' => array(
          'full' => array(
            'label' => 'Full content',
            'custom settings' => FALSE,
          ),
          'teaser' => array(
            'label' => 'Teaser',
            'custom settings' => TRUE,
          ),
          'profile' => array(
            'label' => 'Profile',
            'custom settings' => TRUE,
          ),
          'company_list' => array(
            'label' => 'Company list',
            'custom settings' => TRUE,
          ),
          'email' => array(
            'label' => 'Email',
            'custom settings' => TRUE,
          ),
          'search' => array(
            'label' => 'Search',
            'custom settings' => TRUE,
          ),
        ),
      ),
    ),
    'properties' => array(
      'mid' => array(
        'label' => t('Entity ID'),
        'type' => 'integer',
        'description' => t('The primary identifier for a server.'),
        'schema field' => 'mid',
      ),
      'type' => array(
        'label' => t('Type'),
        'type' => 'text',
        'description' => t('The type of the entity.'),
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'type',
      ),
      'language' => array(
        'label' => t('Language'),
        'type' => 'text',
        'description' => t('The language of the entity.'),
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'language',
      ),
      'title' => array(
        'label' => t('Company name'),
        'type' => 'text',
        'description' => t('The title of the entity.'),
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'title',
      ),
      'uid' => array(
        'label' => t('Uid'),
        'type' => 'integer',
        'description' => t('The user id.'),
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'status' => array(
        'label' => t('Status'),
        'type' => 'boolean',
        'description' => t('The status of the entity.'),
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'status',
      ),
      'created' => array(
        'label' => t('Date created'),
        'type' => 'date',
        'description' => t('The creation date.'),
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'created',
      ),
      'changed' => array(
        'label' => t('Date changed'),
        'type' => 'date',
        'description' => t('The date the item was changed.'),
        'schema field' => 'changed',
      ),
      'sticky' => array(
        'label' => t('Sticky in lists'),
        'type' => 'integer',
        'description' => t('Show as sticky in lists.'),
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'sticky',
      ),
      'crawled_source' => array(
        'label' => t('Source for crawled jobs'),
        'type' => 'text',
      ),
      'imported_type' => array(
        'label' => t('Source for crawled jobs'),
        'type' => 'text',
      ),
      'url' => array(
        'label' => t("URL"),
        'description' => t("The URL of the entity."),
        'getter callback' => 'entity_metadata_entity_get_properties',
        'type' => 'uri',
        'computed' => TRUE,
      ),
    ),
    'fieldable' => TRUE,
  ),
);

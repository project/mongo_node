<?php

/**
 * @file
 * Controller for special loading, saving and menu items.
 */

/**
 * Base class for MongoNode entites.
 */
class MongoNode extends Entity {
  /**
   * Overrides defaultLabel() to provide a better label based on title.
   */
  protected function defaultLabel() {
    return $this->entityInfo['bundles'][$this->bundle()]['label'];
  }

  /**
   * Overrides defaultUri() to provide different uris for entity types.
   */
  protected function defaultUri() {
    return array('path' => $this->entityType . '/' . $this->identifier());
  }

}

/**
 * Mongo Node Controller class.
 */
class MongoNodeController extends EntityApiController {

  /**
   * Constructor as a helper to the parent constructor.
   */
  public function __construct($entity_type) {
    parent::__construct($entity_type);
  }

  /**
   * Loads MongoNode entities directly from MongoDB.
   *
   * @param array $ids
   *   If $ids not provided loads all entities(SearchAPI).
   * @param array $conditions
   *   Conditions for loading entities (use EFQ instead).
   *
   * @return MongoNode
   *   Returns an array of loaded entities indexed by id.
   */
  public function load($ids = array(), $conditions = array()) {
    $entities = array();

    // Return ids of all items (Search API requests this).
    if ($ids === FALSE) {
      $collection = mongodb_collection('fields_current', $this->entityType);
      $result = $collection->find(array('_type' => $this->entityType));

      foreach ($result as $row) {
        $entities[$row['_id']] = $row['_id'];
      }
    }
    else {
      $ids = array_map('intval', $ids);
      $passed_ids = is_array($ids) ? array_flip($ids) : FALSE;

      if ($this->cache) {
        $entities = $this->cacheGet($ids, $conditions);

        if ($passed_ids) {
          $ids = array_keys(array_diff_key($passed_ids, $entities));
        }
      }

      if (empty($ids) && ($this->cacheComplete && !$conditions)) {
        return $entities;
      }

      // Retrieve entities for specific ids.
      $collection = mongodb_collection('fields_current', $this->entityType);
      $result = $collection->find(array('_id' => array('$in' => $ids)));

      static $settings = array();
      $attach_fields = array('image', 'field_collection');

      if (!isset($settings[$this->entityType])) {
        $entity_properties = entity_get_property_info($this->entityType);

        $settings[$this->entityType]['properties'] = array_keys($entity_properties['properties']);
        $settings[$this->entityType]['fields_info'] = field_info_field_by_ids();
        $settings[$this->entityType]['field_instances'] = field_info_instances($this->entityType);
      }

      $properties = $settings[$this->entityType]['properties'];
      $fields_info = $settings[$this->entityType]['fields_info'];
      $field_instances = $settings[$this->entityType]['field_instances'];

      foreach ($result as $row) {
        // Store properties.
        $class = $this->entityInfo['entity class'];
        $entities[$row['_id']] = new $class(array(), $this->entityType);

        foreach ($properties as $property_name) {
          if (isset($row[$property_name])) {
            $entities[$row['_id']]->$property_name = $row[$property_name];
          }
        }
        // Get instances.
        foreach ($field_instances[$row['_bundle']] as $field_name => &$field_info) {
          $field_values = array();
          $field_id = $field_info['field_id'];

          // Store field values.
          if (!empty($row[$field_name])) {
            if ($fields_info[$field_id]['cardinality'] == 1) {
              if (!empty($fields_info[$field_id]['translatable'])) {
                $key = key($row[$field_name]);
                if (!is_numeric($key)) {
                  $field_values[LANGUAGE_NONE] = $row[$field_name];
                  break;
                }

                $tmp = array();

                foreach ($row[$field_name] as $val) {
                  $language = isset($val['_language']) ? $val['_language'] : LANGUAGE_NONE;
                  unset($val['_language']);
                  $tmp[$language][0] = $val;
                }

                $field_values = $tmp;
              }
              else {
                $language = isset($row[$field_name]['_language']) ? $row[$field_name]['_language'] : LANGUAGE_NONE;
                unset($row[$field_name]['_language']);
                $field_values[$language][0] = $row[$field_name];
              }
            }
            else {
              foreach ($row[$field_name] as $column_values) {
                $language = isset($column_values['_language']) ? $column_values['_language'] : LANGUAGE_NONE;

                if (is_array($column_values) && isset($column_values['_language'])) {
                  unset($column_values['_language']);
                }
                $field_values[$language][] = $column_values;
              }
            }
          }

          $entities[$row['_id']]->{$field_name} = $field_values;
          // Some fields need to be attached.
          if (in_array($fields_info[$field_id]['type'], $attach_fields)) {
            $options = array('field_id' => $field_id);
            field_attach_load($this->entityType, $entities, NULL, $options);
          }
        }
      }

      // Pass all entities loaded from mongo through $this->attachLoad(), which
      // calls hook_entity_load().
      if (!empty($entities)) {
        $this->attachLoad($entities);
      }

      if ($this->cache) {
        if (!empty($entities)) {
          $this->cacheSet(array_intersect_key($entities, array_flip($ids)));

          if (!$conditions && $ids === FALSE) {
            $this->cacheComplete = TRUE;
          }
        }
      }
    }

    return $entities;
  }

  /**
   * Override EntityAPIController::attachLoad().
   *
   * Changed to remove attaching the fields and only keeping the call to
   * hook_entity_load.
   */
  protected function attachLoad(&$queried_entities, $revision_id = FALSE) {
    // Call hook_entity_load().
    foreach (module_implements('entity_load') as $module) {
      $function = $module . '_entity_load';
      $function($queried_entities, $this->entityType);
    }
  }

  /**
   * Invoke the field_attach_{operation} function.
   */
  protected function invokeFieldAttach($op, $entity, $entity_type) {
    $function = "field_attach_$op";
    $function($entity_type, $entity);
  }

  /**
   * Invoke entity Hooks.
   */
  protected function invokeEntityHooks($op, $entity, $entity_type) {
    // Invoke the hook.
    module_invoke_all($entity_type . '_' . $op, $entity);
    // Invoke the respective entity level hook.
    module_invoke_all('entity_' . $op, $entity, $entity_type);
    // Invoke rules.
    if (module_exists('rules')) {
      rules_invoke_event($entity_type . '_' . $op, $entity);
    }
  }
  /**
   * Saves a MongoNode entity directly in MongoDB.
   *
   * @param object $entity
   *   MongoNode entity object to save.
   *
   * @return object
   *   Newly saved MongoNode entity.
   */
  public function save($entity) {
    $entity_type = $this->entityType;
    $bundle = $entity->type;

    $entity->is_new = !isset($entity->mid) || empty($entity->mid);
    $entity->status = isset($entity->status) ? $entity->status : 1;
    $entity->language = isset($entity->language) ? $entity->language : LANGUAGE_NONE;
    $entity->uid = isset($entity->uid) ? $entity->uid : 0;

    if (empty($entity->created)) {
      $entity->created = REQUEST_TIME;
    }

    $entity->changed = REQUEST_TIME;

    // @TODO - test entities without bundle.
    if (!(isset($entity->fast_import) && $entity->fast_import)) {
      // field_attach_presave($entity_type, $entity);
      $this->invoke('presave', $entity);
      if (!$entity->is_new) {
        // $entity->original = reset($this->cacheGet(array($entity->mid)));
        $entity->original = entity_load_unchanged($entity_type, $entity->mid);
      }
    }
    else {
      module_invoke_all('entity_fast_presave', $entity, $this->entityType);
    }

    if ($entity->is_new) {
      $op = 'insert';

      // Get a unique id.
      $db = mongodb();
      $id = $db->command(
        array(
          "findandmodify" => $this->entityType . '_ids',
          "query" => array('type' => $this->entityType),
          "update" => array('$inc' => array("c" => 1)),
        )
      );

      if ($id['ok'] && isset($id['value']['c'])) {
        $entity->mid = intval($id['value']['c']);
      }
      else {
        $collection = $db->{$this->entityType . '_ids'};
        $collection->save(array('type' => $this->entityType, 'c' => 2));
        $entity->mid = 1;
      }
    }
    else {
      $op = 'update';
    }

    // Save fields.
    if (!(isset($entity->fast_import) && $entity->fast_import)) {
      $this->invokeFieldAttach($op, $entity, $this->entityType);
    }

    // Create a new object.
    $new_entity = new stdClass();
    $new_entity->_id = intval($entity->mid);
    $new_entity->_type = $this->entityType;
    $new_entity->_bundle = $entity->type;
    $new_entity->status = isset($entity->status) ? $entity->status : 1;
    $new_entity->_language = isset($entity->language) ? $entity->language : LANGUAGE_NONE;
    $new_entity->language = $new_entity->_language;

    // Add the properties of the entity to the new entity.
    static $entity_properties = array();
    if (!isset($entity_properties) || empty($entity_properties)) {
      $entity_properties = entity_get_property_info();
    }

    foreach ($entity_properties[$entity_type]['properties'] as $property_name => &$property_metadata) {
      if (isset($entity->$property_name)) {
        $new_entity->$property_name = _mongo_node_field_storage_value($property_metadata['type'], $entity->$property_name);
      }
    }

    // Retrieve entities for specific ids.
    static $fields_info = array();
    if (!isset($fields_info) || empty($fields_info)) {
      $fields_info = field_info_field_by_ids();
    }

    static $field_instances = array();
    if (!isset($field_instances) || empty($field_instances)) {
      $field_instances = field_info_instances();
    }

    $file_fields = array('file', 'image');

    // Get instances.
    foreach ($field_instances[$entity_type][$bundle] as $field_name => &$field_info) {
      $field_values = array();
      $field_id = $field_info['field_id'];
      $field = $fields_info[$field_id];

      // Store field values.
      if (!empty($entity->$field_name)) {
        foreach ($entity->$field_name as $language => $values) {
          if (empty($values)) {
            continue;
          }

          if ($field['cardinality'] == 1 && empty($field['translatable'])) {
            foreach (reset($values) as $column_name => $column_value) {
              // Add embedded entity support.
              if ($column_name == 'entity') {
                $field_values[$column_name] = _mongo_node_field_storage_value(NULL, $column_value);
              }
              // Ensure that only valid field columns are saved.
              if (!isset($field['columns'][$column_name])) {
                continue;
              }
              $field_values[$column_name] = _mongo_node_field_storage_value($field['columns'][$column_name]['type'], $column_value);
              if ($language != LANGUAGE_NONE) {
                $field_values['_language'] = $language;
              }
            }
          }
          else {
            // Collapse deltas.
            $values = array_values($values);
            if ($field['cardinality'] > 1 && count($values) > $field['cardinality']) {
              throw new MongodbStorageException(
                t('Invalid delta for @field_name, not saving @entity_type @entity_id',
                  array(
                    '@field_name' => $field_name,
                    '@entity_type' => $entity_type,
                    '@entity_id' => $entity->mid,
                  )
                )
              );
            }

            foreach ($values as $column_values) {
              $store_values = array();

              foreach ($column_values as $column_name => $column_value) {
                // Ensure that only valid field columns are saved.
                if (isset($field['columns'][$column_name])) {
                  $store_values[$column_name] = _mongo_node_field_storage_value($field['columns'][$column_name]['type'], $column_values[$column_name]);
                }
                // Add embedded entity support.
                elseif ($column_name == 'entity') {
                  $store_values[$column_name] = _mongo_node_field_storage_value(NULL, $column_values[$column_name]);
                }
              }

              // Collapse the field structure.
              if ($language != LANGUAGE_NONE) {
                $store_values['_language'] = $language;
              }

              $field_values[] = $store_values;
            }

          }

          if (in_array($field['type'], $file_fields)) {
            foreach ($values as $value) {
              $file = (object) $value;

              $is_usage = file_usage_list($file);
              if ($file->fid && !$is_usage) {
                file_usage_add($file, 'file', $entity_type, $new_entity->_id);
              }
            }
          }

          if ($field['type'] == 'geofield' && !empty($field_values['geom'])) {
            $geom = geoPHP::load($field_values['geom'], 'wkb');
            $field_values['geom'] = $geom->out('json');
          }

        }
      }
      $new_entity->$field_name = empty($field_values) ? NULL : $field_values;
    }

    unset($new_entity->is_new);
    if (isset($new_entity->fast_import)) {
      unset($new_entity->fast_import);
    }

    // Save the object.
    try {
      $collection = mongodb_collection('fields_current', $this->entityType);
      $collection->save($new_entity);

      // @TODO - Clear static cache.
      $this->resetCache(array($new_entity->_id));
    }
    catch (Exception $e) {
      watchdog_exception('mongo_node', $e, 'Errro encounterd when saving');
    }

    // Invoke field attach.
    if (!(isset($entity->fast_import) && $entity->fast_import)) {
      $this->invokeEntityHooks($op, $entity, $this->entityType);
    }
    else {
      // Invoke only some hooks.
      module_invoke_all('entity_fast_' . $op, $entity, $this->entityType);
    }

    // Return an object of the entity base class to operate upon.
    $class = $this->entityInfo['entity class'];
    $entity = new $class((array) $new_entity, $this->entityType);

    return $entity;
  }

  /**
   * Deletes MongoDB entities.
   *
   * @param array $ids
   *   An array of entity ids to delete.
   */
  public function delete($ids = array()) {
    $collection = mongodb_collection('fields_current', $this->entityType);

    $entities = $ids ? $this->load($ids) : FALSE;

    if (!$entities) {
      return;
    }

    try {
      $ids = array_keys($entities);

      $collection->remove(array('mid' => array('$in' => $ids)));

      $this->resetCache($ids);

      // TODO -- clear static cache.
      foreach ($entities as $id => $entity) {
        if (module_exists('search')) {
          search_reindex($id, $this->entityType);
        }
        field_attach_delete($this->entityType, $entity);
        $this->invoke('delete', $entity);
      }
    }
    catch (Exception $e) {
      watchdog_exception($this->entityType, $e);
      throw $e;
    }
  }

}

/**
 * MongoNode UI Controller class.
 */
class MongoNodeUIController extends EntityDefaultUIController {
  /**
   * Constructor as a helper to the parent constructor.
   */
  public function __construct($entity_type, $entity_info) {
    parent::__construct($entity_type, $entity_info);
  }

  /**
   * Overrides hook_menu() defaults.
   *
   * Main reason for doing this is that parent class hook_menu() is optimized
   * for entity type administration.
   */
  public function hook_menu() {
    $set = mongo_node_settings();

    $items[$this->path] = array(
      'title' => t('Manage @label', array('@label' => $set[$this->entityType]['label'])),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('mongo_node_admin_content', 3),
      'access callback' => 'mongo_node_menu_access_callback',
      'access arguments' => array("administer $this->entityType entity type"),
      'file' => 'mongo_node.admin.inc',
      'file path' => drupal_get_path('module', 'mongo_node'),
      'type' => MENU_LOCAL_TASK,
    );

    $items[$this->path . '/add'] = array(
      'title' => 'Add entities',
      'description' => 'Add entities',
      'page callback' => 'mongo_node_add_page',
      'access callback' => 'mongo_node_menu_access_callback',
      'access arguments' => array("administer $this->entityType entity type"),
      'file' => 'mongo_node.admin.inc',
      'file path' => drupal_get_path('module', 'mongo_node'),
    );

    // Per entity menus.
    foreach ($set[$this->entityType]['bundles'] as $bundle => $settings) {
      $entity = mongo_node_new($this->entityType, $bundle);
      // Create entity.
      $items[$this->path . '/add/' . $bundle] = array(
        'title' => 'Add ' . $settings['label'],
        'page callback' => 'mongo_node_add',
        'page arguments' => array($this->entityType, $bundle),
        'access callback' => 'mongo_node_access',
        'access arguments' => array('create', $entity),
        'file' => 'mongo_node.admin.inc',
        'file path' => drupal_get_path('module', 'mongo_node'),
      );
    }
    // View entity.
    $items[$this->entityType . '/%' . $this->entityType] = array(
      'title' => 'mongo entity',
      'page callback' => 'mongo_node_page_view',
      'page arguments' => array(0, 1),
      'access callback' => 'mongo_node_access',
      'access arguments' => array('view', 1),
      'load arguments' => array($this->entityType),
      'file' => 'mongo_node.admin.inc',
      'file path' => drupal_get_path('module', 'mongo_node'),
    );
    $items[$this->entityType . '/%' . $this->entityType . '/view'] = array(
      'title' => 'View',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );
    // Edit entity.
    $items[$this->entityType . '/%' . $this->entityType . '/edit'] = array(
      'title' => 'Edit',
      'page callback' => 'mongo_node_page_edit',
      'page arguments' => array(0, 1),
      'access callback' => 'mongo_node_access',
      'access arguments' => array('update', 1),
      'file' => 'mongo_node.admin.inc',
      'file path' => drupal_get_path('module', 'mongo_node'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 0,
    );
    // Delete entity.
    $items[$this->entityType . '/%' . $this->entityType . '/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('mongo_node_page_delete', 0, 1),
      'access callback' => 'mongo_node_access',
      'access arguments' => array('delete', 1),
      'file' => 'mongo_node.admin.inc',
      'file path' => drupal_get_path('module', 'mongo_node'),
      'type' => MENU_LOCAL_TASK,
      'weight' => 1,
    );

    return $items;
  }

}

<?php

/**
 * @file
 * Mongo Node translation handler for the entity translation module.
 */

/**
 * Mongo node translation handler.
 *
 * Overrides default behaviours for Mongo Node entity properties.
 */
class EntityTranslationMongoNodeHandler extends EntityTranslationDefaultHandler {

  /**
   * Constructor as a helper to the parent constructor.
   */
  public function __construct($entity_type, $entity_info, $entity) {
    parent::__construct($entity_type, $entity_info, $entity);
  }

  /**
   * Indicates whether this entity is a revision or not.
   */
  public function isRevision() {
    return FALSE;
  }

  /**
   * Checks whether the current user has access to this entity.
   */
  public function getAccess($op) {
    return mongo_node_access($op, $this->entity);
  }

  /**
   * Always has access to translation.
   */
  public function getTranslationAccess($langcode) {
    return TRUE;
  }

  /**
   * Convert the translation update status fieldset into a vartical tab.
   */
  public function entityForm(&$form, &$form_state) {
    parent::entityForm($form, $form_state);

    // Move the translation fieldset to a vertical tab.
    if (isset($form['translation'])) {
      $form['translation'] += array(
        '#group' => 'additional_settings',
        '#weight' => 100,
        '#attached' => array(
          'js' => array(drupal_get_path('module', 'entity_translation') . '/entity_translation.node-form.js'),
        ),
      );

      if (!$this->isTranslationForm()) {
        $form['translation']['name']['#access'] = FALSE;
        $form['translation']['created']['#access'] = FALSE;
      }
    }

    // Path aliases natively support multilingual values.
    if (isset($form['path'])) {
      $form['path']['#multilingual'] = TRUE;
    }
  }

  /**
   * Provide menu form.
   */
  protected function menuForm(&$form, &$form_state) {
    entity_translation_i18n_menu_form($form, $form_state);
  }

  /**
   * Submit handler for entity form.
   */
  public function entityFormSubmit($form, &$form_state) {
    if (!isset($form_state['values']['translation'])) {
      // Always publish the original values when we have no translations.
      $form_state['values']['translation'] = array('status' => TRUE);
    }
    $values = &$form_state['values']['translation'];

    if (!$this->isTranslationForm()) {
      // Inherit entity authoring information for the original values.
      if (!empty($form_state['values']['date'])) {
        $values['created'] = $form_state['values']['date'];
      }
    }

    parent::entityFormSubmit($form, $form_state);
  }

  /**
   * Provides the form title.
   */
  protected function entityFormTitle() {
    $type_name = node_type_get_name($this->entity);
    return t('<em>Edit @type</em> @title', array('@type' => $type_name, '@title' => $this->getLabel()));
  }

  /**
   * Get the entity status.
   */
  protected function getStatus() {
    return (boolean) $this->entity->status;
  }

  /**
   * Get entity translations.
   */
  public function getTranslations() {
    if ($translations_key = $this->getTranslationsKey()) {
      // Lazy load translations if for some reason the wrapped entity did not go
      // through hook_entity_load().
      if (!isset($this->entity->{$translations_key})) {
        $this->loadTranslations();
      }
      return $this->entity->{$translations_key};
    }
    return self::emptyTranslations();
  }

  /**
   * Returns the translation object key for the wrapped entity type.
   */
  protected function getTranslationsKey() {
    return isset($this->entityInfo['entity keys']['translations']) ? $this->entityInfo['entity keys']['translations'] : FALSE;
  }

}

<?php
/**
 * @file
 * Entity Reference field procesor functions.
 *
 * Save the referenced entity along in the field.
 */

$plugin = array(
  'label' => 'Entity Reference',
  'entity_save' => 'mongo_node_embed_entity_save_entityreference',
  'entity_load' => 'mongo_node_embed_entity_load_entityreference',
);


/**
 * Process function for saving 'entityreference' field values.
 */
function mongo_node_embed_entity_save_entityreference($entity_type, $entity, $field, &$values) {
  $target_type = $field['settings']['target_type'];
  if ($field['cardinality'] == 1) {
    if ($target_entity = entity_load_single($target_type, $values['target_id'])) {
      $values['entity'] = mongo_node_embed_entity_to_doc($target_entity, $target_type);
    }
  }
  else {
    foreach ($values as &$value) {
      if ($target_entity = entity_load_single($target_type, $value['target_id'])) {
        $value['entity'] = mongo_node_embed_entity_to_doc($target_entity, $target_type);
      }
    }
  }
}

/**
 * Process function for loading 'entityreference' field values.
 */
function mongo_node_embed_entity_load_entityreference($entity_type, $entity, $field, &$field_values) {
  // This only works if entityreference module is patched. Otherwise referenced
  // entities are loaded again anyway, in entityreference prepare_view hook.
  // @TODO: check for updates here https://www.drupal.org/node/2289793 .
  $target_type = $field['settings']['target_type'];
  foreach ($field_values as &$values) {
    foreach ($values as &$value) {
      if (isset($value['entity']) && is_array($value['entity'])) {
        $val = mongo_node_embed_doc_to_entity($value['entity'], $target_type);
        $entity = entity_create($target_type, $val);
        unset($entity->is_new);
        $value['entity'] = $entity;
      }
    }
  }
}

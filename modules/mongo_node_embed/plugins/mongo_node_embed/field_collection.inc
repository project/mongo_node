<?php

/**
 * @file
 * Field Collection field procesor functions.
 *
 * Save the field collection entity along with the field values.
 */

$plugin = array(
  'label' => 'Field Collection',
  'entity_save' => 'mongo_node_embed_field_save_field_collection',
  'entity_load' => 'mongo_node_embed_field_load_field_collection',
);

/**
 * Process function for 'field_collection' field values.
 *
 * Prepare an object to be stored in MongoDB along the collection ID.
 */
function mongo_node_embed_field_save_field_collection($entity_type, $entity, $field, &$values) {
  $target_type = 'field_collection_item';
  if (!is_array($values)) {
    return;
  }
  // Regardless of the field cardinality, we always get the same structure here.
  foreach ($values as &$item) {
    foreach ($item as &$value) {
      if (isset($value['value']) && $target_entity = entity_load_single($target_type, $value['value'])) {
        $value['entity'] = mongo_node_embed_entity_to_doc($target_entity, $target_type);
      }
    }
  }
}

/**
 * Process function for 'field_collection' field values.
 *
 * Save all field collection entities as instances of FieldCollectionItemEntity.
 */
function mongo_node_embed_field_load_field_collection($entity_type, $entity, $field, &$field_values) {
  foreach ($field_values as &$values) {
    if (!is_array($values)) {
      continue;
    }
    foreach ($values as &$value) {
      if (isset($value['entity']) && is_array($value['entity'])) {
        $val = mongo_node_embed_doc_to_entity($value['entity'], 'field_collection_item');
        $entity = new FieldCollectionItemEntity($val);
        unset($entity->is_new);
        $value['entity'] = $entity;
      }
    }
  }
}

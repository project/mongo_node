<?php

/**
 * @file
 * Define the custom OG Mongo Node selection handler plugin.
 */

$plugin = array(
  'title' => t('OG Mongo Node'),
  'class' => 'OgMongoNodeSelectionHandler',
);

<?php

/**
 * @file
 * Custom Organic Groups selection handler for Mongo Node.
 */

module_load_include('php', 'entityreference', 'plugins/selection/EntityReference_SelectionHandler_Generic.class');

/**
 * OG selection handler.
 */
class OgMongoNodeSelectionHandler extends EntityReference_SelectionHandler_Generic {

  /**
   * Implements EntityReferenceHandler::getInstance().
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    return new OgMongoNodeSelectionHandler($field, $instance, $entity_type, $entity);
  }

  /**
   * Override EntityReferenceHandler::settingsForm().
   */
  public static function settingsForm($field, $instance) {
    $form = parent::settingsForm($field, $instance);

    $entity_type = $field['settings']['target_type'];
    $entity_info = entity_get_info($entity_type);

    // This handler only works for Mongo node entities.
    if (isset($entity_info['module']) && $entity_info['module'] != 'mongo_node') {
      $form['target_bundles'] = array(
        '#type' => 'item',
        '#title' => t('Target bundles'),
        '#markup' => t('Error: The selected "Target type" %entity is not a mongo node entity type.', array('%entity' => $entity_info['label'])),
      );
      return $form;
    }

    $bundles = array();
    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      if (og_is_group_type($entity_type, $bundle_name)) {
        $bundles[$bundle_name] = $bundle_info['label'];
      }
    }

    if (!$bundles) {
      $form['target_bundles'] = array(
        '#type' => 'item',
        '#title' => t('Target bundles'),
        '#markup' => t('Error: The selected "Target type" %entity does not have bundles that are a group type', array('%entity' => $entity_info['label'])),
      );
    }
    else {
      $settings = $field['settings']['handler_settings'];
      $settings += array(
        'target_bundles' => array(),
        'membership_type' => OG_MEMBERSHIP_TYPE_DEFAULT,
      );

      $form['target_bundles'] = array(
        '#type' => 'select',
        '#title' => t('Target bundles'),
        '#options' => $bundles,
        '#default_value' => $settings['target_bundles'],
        '#size' => 6,
        '#multiple' => TRUE,
        '#description' => t('The bundles of the entity type acting as group, that can be referenced. Optional, leave empty for all bundles.'),
      );

      $options = array();
      foreach (og_membership_type_load() as $og_membership) {
        $options[$og_membership->name] = $og_membership->description;
      }
      $form['membership_type'] = array(
        '#type' => 'select',
        '#title' => t('OG membership type'),
        '#description' => t('Select the membership type that will be used for a subscribing user.'),
        '#options' => $options,
        '#default_value' => $settings['membership_type'],
        '#required' => TRUE,
      );
    }

    return $form;
  }

  /**
   * Build an EntityFieldQuery to get referencable entities.
   *
   * Inspider from OgSelectionHandler.
   */
  public function buildEntityFieldQuery($match = NULL, $match_operator = 'CONTAINS') {
    global $user;

    $query = parent::buildEntityFieldQuery($match, $match_operator);

    $group_type = $this->field['settings']['target_type'];
    $entity_info = entity_get_info($group_type);

    // FIXME: http://drupal.org/node/1325628
    unset($query->tags['node_access']);

    // FIXME: drupal.org/node/1413108
    unset($query->tags['entityreference']);

    $query->addTag('entity_field_access');
    $query->addTag('og');

    if (!field_info_field(OG_GROUP_FIELD)) {
      // There are no groups, so falsify query.
      $query->propertyCondition($entity_info['entity keys']['id'], -1, '=');
      return $query;
    }

    // Show only the entities that are active groups.
    $query->fieldCondition(OG_GROUP_FIELD, 'value', 1, '=');
    if (empty($this->instance['field_mode'])) {
      return $query;
    }

    // Fix the bundle array value.
    if (isset($query->entityConditions['bundle']['value']) && !empty($query->entityConditions['bundle']['value'])) {
      $query->entityConditions['bundle']['value'] = array_keys($query->entityConditions['bundle']['value']);
    }

    $field_mode = $this->instance['field_mode'];
    $user_groups = og_get_groups_by_user(NULL, $group_type);
    $user_groups = $user_groups ? $user_groups : array();

    // Mongo_node entity ID-s must be passed as int, otherwise the query fails.
    if ($user_groups) {
      $user_groups = array_map('intval', $user_groups);
    }

    // Show the user only the groups they belong to.
    if ($field_mode == 'default') {
      if ($user_groups) {
        $entity_type = $this->instance['entity_type'];
        $bundle = $this->instance['bundle'];

        $entity = $this->entity;
        $ids = array();
        foreach ($user_groups as $gid) {
          // Check if user has 'administer' permissions on those groups.
          if (og_user_access($group_type, $gid, "administer {$entity_type} entities")) {
            $ids[] = $gid;
          }
          // Check if user has 'create' permissions on those groups.
          elseif (og_user_access($group_type, $gid, "create {$entity_type} entities")) {
            $ids[] = $gid;
          }
          // Check if the entity exists and the user has edit permission.
          elseif (isset($entity->mid) && !empty($entity->mid)) {
            // Check for any.
            if (og_user_access($group_type, $gid, "edit any {$entity_type} entities")) {
              $ids[] = $gid;
            }
            // Check for own.
            elseif ($entity->uid == $user->uid && og_user_access($group_type, $gid, "edit own {$entity_type} entities")) {
              $ids[] = $gid;
            }
          }
        }
      }
      else {
        $ids = $user_groups;
      }

      if ($ids) {
        $query->propertyCondition($entity_info['entity keys']['id'], $ids, 'IN');
      }
      else {
        // User doesn't have permission to select any group so falsify this
        // query.
        $query->propertyCondition($entity_info['entity keys']['id'], -1, '=');
      }
    }
    elseif ($field_mode == 'admin' && $user_groups) {
      // Show only groups the user doesn't belong to.
      if ($user_groups) {
        $query->propertyCondition($entity_info['entity keys']['id'], $user_groups, 'NOT IN');
      }
    }

    return $query;
  }

}

<?php $settings = array (
  'mongo_article' => 
  array (
    'permissions' => 1,
    'label' => 'Article',
    'bundles' => 
    array (
      'article_per_link' => 
      array (
        'label' => 'Article per link',
        'description' => '',
        'view modes' => 
        array (
          'full' => 
          array (
            'label' => 'Full content',
            'custom settings' => false,
          ),
          'teaser' => 
          array (
            'label' => 'Teaser',
            'custom settings' => true,
          ),
        ),
      ),
      'article_per_description' => 
      array (
        'label' => 'Article per template',
        'description' => '',
        'view modes' => 
        array (
          'full' => 
          array (
            'label' => 'Full content',
            'custom settings' => false,
          ),
          'teaser' => 
          array (
            'label' => 'Teaser',
            'custom settings' => true,
          ),
        ),
      ),
    ),
    'fieldable' => true,
    'uuid' => 0,
  ),
  'company' => 
  array (
    'label' => 'Company',
    'bundles' => 
    array (
      'company' => 
      array (
        'label' => 'Company',
        'description' => '',
        'view modes' => 
        array (
          'full' => 
          array (
            'label' => 'Full content',
            'custom settings' => false,
          ),
          'teaser' => 
          array (
            'label' => 'Teaser',
            'custom settings' => true,
          ),
          'profile' => 
          array (
            'label' => 'Profile',
            'custom settings' => true,
          ),
          'company_list' => 
          array (
            'label' => 'Company list',
            'custom settings' => true,
          ),
          'email' => 
          array (
            'label' => 'Email',
            'custom settings' => true,
          ),
          'search' => 
          array (
            'label' => 'Search',
            'custom settings' => true,
          ),
        ),
      ),
    ),
    'properties' => 
    array (
      'mid' => 
      array (
        'label' => 'Entity ID',
        'type' => 'integer',
        'description' => 'The primary identifier for a server.',
        'schema field' => 'mid',
      ),
      'type' => 
      array (
        'label' => 'Type',
        'type' => 'text',
        'description' => 'The type of the entity.',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'type',
      ),
      'language' => 
      array (
        'label' => 'Language',
        'type' => 'text',
        'description' => 'The language of the entity.',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'language',
      ),
      'title' => 
      array (
        'label' => 'Company name',
        'type' => 'text',
        'description' => 'The title of the entity.',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'title',
      ),
      'uid' => 
      array (
        'label' => 'Uid',
        'type' => 'integer',
        'description' => 'The user id.',
        'setter callback' => 'entity_property_verbatim_set',
      ),
      'status' => 
      array (
        'label' => 'Status',
        'type' => 'boolean',
        'description' => 'The status of the entity.',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'status',
      ),
      'created' => 
      array (
        'label' => 'Date created',
        'type' => 'date',
        'description' => 'The creation date.',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'created',
      ),
      'changed' => 
      array (
        'label' => 'Date changed',
        'type' => 'date',
        'description' => 'The date the item was changed.',
        'schema field' => 'changed',
      ),
      'sticky' => 
      array (
        'label' => 'Sticky in lists',
        'type' => 'integer',
        'description' => 'Show as sticky in lists.',
        'setter callback' => 'entity_property_verbatim_set',
        'schema field' => 'sticky',
      ),
      'crawled_source' => 
      array (
        'label' => 'Source for crawled jobs',
        'type' => 'text',
      ),
      'imported_type' => 
      array (
        'label' => 'Source for crawled jobs',
        'type' => 'text',
      ),
      'url' => 
      array (
        'label' => 'URL',
        'description' => 'The URL of the entity.',
        'getter callback' => 'entity_metadata_entity_get_properties',
        'type' => 'uri',
        'computed' => true,
      ),
    ),
    'fieldable' => true,
    'uuid' => 1,
    'permissions' => 0,
  ),
);
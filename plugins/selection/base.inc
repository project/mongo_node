<?php

/**
 * @file
 * EntityReference Selection Handler plugin Mongo Node.
 */

$plugin = array(
  'title' => t('Basic'),
  'class' => 'MongoNodeSelectionHandler',
  'weight' => -100,
);

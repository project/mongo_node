<?php
/**
 * @file
 * EntityReference Selection Handler Class.
 */

include_once drupal_get_path('module', 'entityreference') . '/plugins/selection/EntityReference_SelectionHandler_Generic.class.php';

/**
 * A EntityReference SelectionHandler for MongoEntity.
 */
class MongoNodeSelectionHandler extends EntityReference_SelectionHandler_Generic {

  /**
   * Implements EntityReferenceHandler::getInstance().
   */
  public static function getInstance($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    // Empty base tables are OK by me.
    return new MongoNodeSelectionHandler($field, $instance, $entity_type, $entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function __construct($field, $instance = NULL, $entity_type = NULL, $entity = NULL) {
    $this->field = $field;
    $this->instance = $instance;
    $this->entity_type = $entity_type;
    $this->entity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function buildEntityFieldQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityFieldQuery($match, $match_operator);

    // Fix the bundle array value.
    if (isset($query->entityConditions['bundle']['value']) && !empty($query->entityConditions['bundle']['value'])) {
      $query->entityConditions['bundle']['value'] = array_keys($query->entityConditions['bundle']['value']);
    }

    return $query;
  }

}

<?php

/**
 * @file
 * Hooks provided by the Mongo Node module.
 */

/**
 * Control access to a mongo node entity.
 *
 * Modules may implement this hook if they want to have a say in whether or not
 * a given user has access to perform a given operation to the entity. We use
 * the same implementation as Entity API does.
 *
 * @param string $op
 *   The operation to be performed. Possible values:
 *   - "create"
 *   - "delete"
 *   - "update"
 *   - "view".
 * @param object $entity
 *   The Mongo Node entity object.
 * @param object $account
 *   The user object to perform the access check operation on.
 * @param string $entity_type
 *   The entity type.
 *
 * @return constant
 *   - MONGO_NODE_ACCESS_ALLOW: if the operation is to be allowed.
 *   - MONGO_NODE_ACCESS_DENY: if the operation is to be denied.
 *
 * @see entity_access()
 */
function hook_mongo_node_access($op, $entity, $account, $entity_type) {
  // Allow everyone to create fancy entities.
  if ($op == 'create' && $entity_type == 'fancy_entity') {
    return MONGO_NODE_ACCESS_ALLOW;
  }
}
